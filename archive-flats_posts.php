<?php 
/*  
    Template Name: Dzīvokļi
*/
?>
<?php
get_header();?>
<?php $current = get_post_type_archive_link('flats_posts'); ?>
<?php if(isset($_GET['floor'])){
	$checklist = $_GET['floor'] ;
};
if(isset($_GET['rooms'])){
	$rooms = $_GET['rooms'] ;
};
if(isset($_GET['area'])){
	$area = $_GET['area'] ;
};
if(isset($_GET['statuss'])){
	$statuss = $_GET['statuss'] ;
};?>

<section class="filters options">
    <div class="container">
        <form name="form"  id="form" method="post">
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="form-group">
                        <label><?php echo _e('Stāvs','marupesnami');?></label>
                        <div class="has-checkbox d-flex">
							<?php $terms = get_terms( 'stavs');
							$i = 0;
							foreach ( $terms as $term ) {
								$i++;?>
								<?php if(isset($_GET['floor']) || isset($_GET['rooms']) || isset($_GET['area']) || isset($_GET['statuss'])){?>
									<input type="checkbox" form="form" id="cb-<?php echo $i;?>" name="check_list[]" value="<?php echo $term_slug = $term->slug;?>" <?php if(strpos($checklist, $term_slug) !== FALSE ){ echo 'checked';};?>>
								<?php }else{?>
									<input type="checkbox" form="form" id="cb-<?php echo $i;?>" name="check_list[]" value="<?php echo $term_slug = $term->slug;?>">
								<?php };?>
								<?php echo '<label for="cb-'.$i.'">'.$term->name.'</label>';
							}?>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="form-group">
                        <label><?php echo _e('Istabu skaits','marupesnami');?></label>

                        <div class="has-checkbox d-flex">
                        	<?php $terms_tax = get_terms( 'istabas');
							$a = 0;
							foreach ( $terms_tax as $tax ) {
								$a++;?>
                                <?php if(isset($_GET['floor']) || isset($_GET['rooms']) || isset($_GET['area']) || isset($_GET['statuss'])){?>
									<input type="checkbox" form="form" id="cg-<?php echo $a;?>" name="rooms[]" value="<?php echo $tax_slug = $tax->slug;?>" <?php if(strpos($rooms, $tax_slug) !== FALSE ){ echo 'checked';};?>>
								<?php }else{?>
									<input type="checkbox" form="form" id="cg-<?php echo $a;?>" name="rooms[]" value="<?php echo $tax_slug = $tax->slug;?>">
								<?php };?>
								<?php echo '<label for="cg-'.$a.'">'.$tax->name.'</label>';
							};?>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 col-md-6">
                	<?php $terms_area = get_terms('area');
						?>

                    <div class="form-group platiba">
                        <label><?php echo _e('Platība','marupesnami');?></label>
						<select class="selectpicker" value="" title="<?php if($_GET['area'] ){ $tag = get_term_by('slug',$_GET['area'], 'area');echo $tag->name;}else{echo _e('Platība','marupesnami');};?>">
							<option><?php echo _e('Visi','marupesnami');?></option>
                        	<?php foreach ( $terms_area as $tax_area ) {?>

                            	<option class="platiba_values" value="<?php echo $tax_slug = $tax_area->slug;?>" <?php if(strpos(isset($_GET['area']), $tax_slug) !== FALSE ){ echo 'selected';};?>><?php echo $tax_area->name;?></option>
                         	<?php };?>

                        </select>

                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="form-group statuss">
                        <label><?php echo _e('Statuss','marupesnami');?></label>

                        <select class="selectpicker" title="<?php if($_GET['area'] ){ $tag_statuss = get_term_by('slug',$_GET['statuss'], 'statuss');echo $tag_statuss->name;}else{echo _e('Statuss','marupesnami');};?>">
                        	<option><?php echo _e('Visi','marupesnami');?></option>
                            <?php $terms_statuss = get_terms('statuss');
							foreach ( $terms_statuss as $tax_statuss ) {?>
                            	<option class="statuss_values" value="<?php echo $statuss_slug = $tax_statuss->slug;?>" <?php if(strpos(isset($_GET['statuss']), $statuss_slug) !== FALSE ){ echo 'selected';};?>><?php echo _e($tax_statuss->name,'marupesnami');?></option>
       
                            <?php };?>
                        </select>
                    </div>
                </div>
            </div>
        </form>
       <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

		<script>
			$(document).ready(function () {
			  	$('form').on('change',function (e) {
			  		var seasoning = '', tempArray = [];
			  		$('input[name="check_list[]"]:checked').each(function(){
			      		tempArray.push($(this).val());
			  		})
				  	if(tempArray.length !== 0){
				     	seasoning+='floor='+tempArray.toString();
				     	tempArray = [];
				  	}
				  	$('input[name="rooms[]"]:checked').each(function(){
				      	tempArray.push($(this).val());
				  	})
				  	if(tempArray.length !== 0){
				     	seasoning+='&rooms='+tempArray.toString();
				        tempArray = [];

				  	}
				  	var text = $('.platiba .filter-option.pull-left').text();
				  	$('.platiba .platiba_values').each(function(){
				    var option = $(this).text();
				    if(option==text){
				    	var value =$(this).val();
				    	if(value){
				      		tempArray.push(value);
				      	}
				    }
					})
				  	if(tempArray.length !== 0){
				     	seasoning+='&area='+tempArray.toString();
				        tempArray = [];

				  	}
				  	var text = $('.statuss .filter-option.pull-left').text();
				  	$('.statuss .statuss_values').each(function(){
				    var option = $(this).text();
				    if(option==text){
				    	var value = $(this).val();
				    	if(value){
				      		tempArray.push(value);
				      	}
				    }
					})
				  	if(tempArray.length !== 0){
				     	seasoning+='&statuss='+tempArray.toString();
				        tempArray = [];

				  	}

				  	window.location = '<?php echo $current;?>?'+seasoning;

			  	});
			});
		</script>
    </div>
</section>
<div style="">
    <?php 
		$tax_query = array('relation' => 'AND');
		if(isset($_GET['floor'])){
 			$tax_query[] =  array(
	            'taxonomy' => 'stavs',
	            'field' => 'slug',
	           	'terms' =>  explode(",",$checklist),
	           	'operators' => 'or'
        	);

        };
		if(isset($_GET['rooms'])){
	
		   	$tax_query[] =  array(
	            'taxonomy' => 'istabas',
	            'field' => 'slug',
	           	'terms' => explode(",",$rooms),
	           	'operators' => 'or',

        	);
  

        };
    	if(isset($_GET['area'])){
        	$tax_query[] =  array(
	            'taxonomy' => 'area',
	            'field' => 'slug',
	            'terms' => $area,
        	);
    	};
        if(isset($_GET['statuss'])){

        	$tax_query[] =  array(
	            'taxonomy' => 'statuss',
	            'field' => 'slug',
	            'terms' => $statuss,
        	);
        };

     	
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

	    $query = new WP_Query(array('posts_per_page' => 10 ,
	        'post_type' 		=> 'flats_posts',

	        'tax_query' => $tax_query,
			'paged' => $paged,

		));
		$page_count = $query->max_num_pages;
	    $post_count = $query->post_count;

	?>
</div>
<?php if($query->have_posts()) : $counter=0;?> 
    <section class="filters results">
        <div class="container">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nr.</th>
                        <th><?php echo _e('Stāvs','marupesnami');?></th>
                        <th><?php echo _e('Istabu skaits','marupesnami');?></th>
                        <th><?php echo _e('Platība','marupesnami');?></th>
                        <th><?php echo _e('Cena','marupesnami');?></th>
                        <th><?php echo _e('Statuss','marupesnami');?></th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
			
                <tbody>
            		<?php while ( $query->have_posts()) : $query->the_post(); $counter++;?>
	            		<tr><?php $posts_per_page = get_option( 'posts_per_page' );?>

	                        <td><?php the_field('flats_number');?></td>
	                        <td><?php $terms_stavs = get_the_terms( get_the_ID(), 'stavs' );
	                            $n = 0;
	                            $len = count($terms_stavs);
	                            if($terms_stavs){
		                        	foreach($terms_stavs as $stavs_term){
		                        		$n++;
		                        		if ($n == $len) {
		                        			echo $stavs_term->name;
		                        		}else{
		                        			echo $stavs_term->name.',';
		                        		}
		                        	};
		                        };?>                                		
	                        </td>
	                        <td><?php $terms_istabas = get_the_terms( get_the_ID(), 'istabas' );
		                        if($terms_istabas){
		                        	foreach($terms_istabas as $rooms_term){
		                        		echo $rooms_term->name;
		                        	}
		                        };?>                                		
	                        </td>
	                        <td><?php  echo get_field('area_number').' m2';?></td>
	                        <td><?php the_field('flats_price');?></td>
	                        <td><?php $statuss_terms = get_the_terms( get_the_ID(), 'statuss' );
		                        if($statuss_terms){
		                        	foreach($statuss_terms as $statuss_tax){
		                        		echo _e($statuss_tax->name,'marupesnami');
		                        	}
		                        };?>                                		
	                        </td>
							<td>
	                            <a href="<?php the_permalink();?>" class="btn btn-primary"><?php echo _e('Apskatīt','marupesnami');?></a>
	                        </td>
	                    </tr>
                    <?php endwhile;?>
            	</tbody>
            </table>

        </div>

        <?php if($page_count >1 ){?>
           <?php simple_pagination(); ?>
		<?php };?>
    </section>
 <?php endif;?>
 <?php require get_template_directory() . '/views/contact-form.php';?>

<?php get_footer();
?>