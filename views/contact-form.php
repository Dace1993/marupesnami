<section class="contact-us form"> 
    <div class="container">
        <h2 class="section-name"><?php echo _e('Sazinies ar mums','marupesnami');?></h2>

        <div class="row">
            <div class="form-col col-lg-6">
                <?php $shortcode = __("[ninja_form id=1]",'marupesnami');
                echo do_shortcode($shortcode);?>
            </div>
            <?php $contact_persons = get_field('contact_persons','option');
            if($contact_persons){?>
                <div class="persons-col col-lg-6 d-lg-flex justify-content-end">
                    <div class="contact-persons">
                        <?php foreach($contact_persons as $person){?>
                            <div class="person d-flex align-items-center">
                                <div class="photo-thumbnail">
                                    <?php $person_thumb = $person['image'];
                                    $image_size = 'contactperson_thumb';
                                    $image_url = $person_thumb['sizes'][ $image_size];?>
                                    <img src="<?php echo $image_url;?>" alt="Vārds Uzvārds">
                                </div>

                                <div class="details">
                                    <h4 class="name"><?php echo _e($person['name_surname'],'marupesnami');?></h4>
                                    <span class="position"><?php echo _e($person['position'],'marupesnami');?></span>

                                    <ul class="contacts">
                                        <li><a href="mailto:<?php echo $person['mail'];?>"><?php echo $person['mail'];?></a></li>
                                        <li><a href="tel:<?php echo $person['phone'];?>"><?php echo $person['phone'];?></a></li>
                                    </ul>
                                </div>
                            </div>
                        <?php };?>
                    </div>
                </div>
            <?php };?>
        </div>

        <a href="#" class="scroll-down white-arrow right"></a>
    </div>
</section>