<?php 
/*  
    Template Name: Home
*/
?>

<?php get_header(); ?>
<?php $hero_slider = get_field('hero_slider');
if($hero_slider){?>
    <section class="hero-slider">
        <div class="owl-carousel">
            <?php foreach($hero_slider as $hero){?>
                <div class="d-flex align-items-center" style="background: url('<?php echo $hero['image'];?>') 0 0;">
                    <div class="container">
                        <div class="text">
                            <h1><?php echo $hero['title'];?> <span><?php echo $hero['sub_title'];?></span></h1>
                            <p><?php echo $hero['text'];?></p>
                            
                            <?php if($hero['button']){?>
                                <a href="<?php echo $hero['button']['url'];?>" class="btn btn-transpatent"><?php echo $hero['button']['title'];?></a>
                            <?php };?>
                        </div>
                    </div>
                </div>
            <?php };?>
        </div>
    </section>
<?php };?>
<main id="main-content">
    <?php $project_block = get_field('project_block');
    if($project_block){?>
        <section class="about-us project">
            <div class="container">
                <h2 class="section-name"><?php echo $project_block['block_title'];?></h2>
                <?php $about_project = $project_block['about_project'];
                if($about_project){?>
                    <div class="tabs-block">
                        <div class="row justify-content-end">
                            <div class="col-lg-9">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <?php $i = 0;
                                    foreach ($about_project as $project) {
                                        $i++;?>
                                        <li class="nav-item">
                                            <a class="nav-link <?php if($i==1){echo 'active';};?>" data-toggle="tab" href="#project-tab-<?php echo $i;?>" role="tab" aria-expanded="<?php if($i==1){echo 'true';}else{echo 'false';};?>"><?php echo $project['tab_title'];?></a>
                                        </li>
                                    <?php };?>
                                </ul>
                            </div>
                        </div>

                        <div class="tab-content">
                            <?php $x = 0;
                            foreach ($about_project as $projects) {
                                $x++;?>
                                <div class="tab-pane fade <?php if($x==1){echo 'show active';};?>" id="project-tab-<?php echo $x;?>" role="tabpanel">
                                    <div class="row">
                                        <div class="col-lg-9 offset-lg-3">
                                            <?php echo '<p>'.$projects['text'].'</p>';?>
                                            <?php if($projects['link']){?>
                                                <div class="read-more text-lg-right">
                                                    <a href="<?php echo $projects['link'];?>" class="btn btn-primary"><?php echo _e('UZZINI VAIRĀK','marupesnami');?></a>
                                                </div>
                                            <?php };?>
                                        </div>

                                        <div class="col-12">
                                            <div class="image">
                                                <?php $project_thumb = $projects['image'];
                                                $image_size = 'project_thumb';
                                                $image_url = $project_thumb['sizes'][ $image_size];?>
                                                <img src="<?php echo $image_url;?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php };?>
                        </div>
                    </div>
                <?php };?>
                <a href="#" class="scroll-down green-arrow right"></a>
            </div>
        </section>
    <?php };?>
    <?php $flats_block = get_field('flats_block');
    if($flats_block){?>
        <section class="about-us apartments">
            <div class="container">
                <h2 class="section-name"><?php echo $flats_block['block_title'];?></h2>
                <?php $about_flats = $flats_block['about_flats'];
                if($about_flats){?>
                    <div class="tabs-block">
                        <div class="row justify-content-end">
                            <div class="col-lg-9">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <?php $y = 0;
                                    foreach ($about_flats as $flats) {
                                        $y++;?>
                                        <li class="nav-item">
                                            <a class="nav-link <?php if($y==1){echo 'active';};?>" data-toggle="tab" href="#apartments-tab-<?php echo $y;?>" role="tab" aria-expanded="<?php if($y==1){echo 'true';}else{echo 'false';};?>"><?php echo $flats['tab_title'];?></a>
                                        </li>
                                    <?php };?>
                                </ul>
                            </div>
                        </div>

                        <div class="tab-content">
                            <?php $a = 0;
                            $flats_about = $flats_block['about_flats'];
                            if($flats_about){
                                foreach ($flats_about as $flat) {
                                    $a++;?>
                                    <div class="tab-pane fade <?php if($a==1){echo 'show active';};?>" id="apartments-tab-<?php echo $a;?>" role="tabpanel">
                                        <div class="row">
                                            <div class="col-lg-9 offset-lg-3">
                                                <?php echo '<p>'.$flat['text'].'</p>';?>

                                            </div>
                                            <?php wp_reset_query(); ?>
                                           <?php $flats_posts = $flat['flats_posts'];
                                            if( $flats_posts ){?>
                                                <div class="col-12">
                                                    <?php foreach( $flats_posts as $flats_room){?>
                                                        <div class="apartment-ad">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="thumbnail">
                                                                        <?php $flats_thumb = $flats_room['image'];
                                                                        $images_size= 'flats_thumb';
                                                                        $images_url = $flats_thumb['sizes'][$images_size];?>
                                                                        <img src="<?php echo $images_url;?>">
                                                                        <?php if($flats_room['link']){?>
                                                                            <a href="<?php echo $flats_room['link'];?>" class="btn btn-primary"><?php echo _e('UZZINI VAIRĀK','marupesnami');?></a>
                                                                        <?php };?>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="details">
                                                                        <div class="d-sm-flex justify-content-between align-items-center">
                                                                            <div class="rooms">
                                                                                <?php  $room_id = $flats_room['rooms_taxonomy'];
                                                                                $terms_istabas = get_term_by( 'id',  $room_id[0], 'istabas' );
                                                                                $terms_istabas_name = $terms_istabas->name;
                                                                                echo '<a href="'.get_home_url().'/?rooms='.$terms_istabas->slug.'&statuss=brivs">'.$terms_istabas_name;
                                                                                $one = '1';
                                                                                if($terms_istabas_name !== $one){?>
                                                                                    <?php echo _e('Istabas','marupesnami').'</a>';?>
                                                                                <?php }else{?>
                                                                                    <?php echo _e('Istaba','marupesnami').'</a>';?>
                                                                                <?php };?>
                                                                            </div>
                                                                            <?php $tax_query = array('relation' => 'AND');
                                                                                $tax_query[] =  array(
                                                                                'taxonomy' => 'statuss',
                                                                                'field' => 'slug',
                                                                                'terms' => 'brivs',
                                                                            );
                                                                             $tax_query[] =  array(
                                                                                'taxonomy' => 'istabas',
                                                                                'field' => 'slug',
                                                                                'terms' => $terms_istabas->slug,
                                                                            );
                                                                            $args = array(
                                                                                'posts_per_page'    => 100,
                                                                                'orderby'          => 'date',
                                                                                'order'            => 'DESC',
                                                                                'post_type'        => 'flats_posts',
                                                                                'post_status'      => 'publish',
                                                                                'lang'             => pll_default_language(),

                                                                                'tax_query'=>$tax_query,
                                                                            );
                                                                            $query = new WP_Query($args);
                                                                            $post_count = $query->post_count;

                                                                            if($post_count > 1){
                                                                                echo '<div class="total"><a href="'.get_home_url().'/?rooms='.$terms_istabas->slug.'&statuss=brivs">'; echo _e('Pieejami','marupesnami').' '.$post_count.' '; echo _e('dzīvokļi','marupesnami').'</a></div>';
                                                                            }else{
                                                                                echo '<div class="total"><a href="'.get_home_url().'/?rooms='.$terms_istabas->slug.'&statuss=brivs">'; echo _e('Pieejams','marupesnami').' '.$post_count.' '; echo _e('dzīvoklis','marupesnami').'</a></div>';
                                                                            };?> 

                                                                        </div>

                                                                        <div class="description">
                                                                            <p><?php echo $flats_room['rooms_short_description'];?></p>

                                                                        </div>

                                                                        <ul class="features">
                                                                            <?php if($flats_room['number_area']){?>
                                                                                <li>
                                                                                    <?php echo _e('Platība','marupesnami'); echo ': '.$flats_room['number_area'].' m2'; ?>   
                                                                                </li>
                                                                            <?php };?>
                                                                            <li><?php echo _e('Pieejamie stāvi','marupesnami');?>: 
                                                                                <?php $floor_category = $flats_room['floor_category'];
                                                                                if($floor_category){
                                                                                    $n = 0;
                                                                                    $len = count($floor_category);
                                                                                    foreach($floor_category as $category_floor){
                                                                                        $terms_stavi = get_term_by( 'id',  $category_floor, 'stavs' );
                                                                                        $n++;
                                                                                        if ($n == $len) {
                                                                                            echo $terms_stavi->name.'.';
                                                                                        }else{
                                                                                            echo $terms_stavi->name.'., ';
                                                                                        }
                                                                                    }
                                                                                }
                                                                              ?>         
                                                                            </li>
                                                                        </ul>

                                                                        <div class="price text-lg-right">
                                                                            <strong><?php echo _e('CENA NO','marupesnami');?>: <?php echo $flats_room['room_price'];?> Eur</strong>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php };?>
                                                </div>
                                            <?php };?>
                                        </div>
                                    </div>
                                <?php };
                            };?>
                        </div>
                    </div>
                <?php };?>
                <a href="#" class="scroll-down white-arrow left"></a>
            </div>
        </section>
    <?php };?>
    <?php wp_reset_query(); ?>
    <?php $location_block = get_field('location_block');
    if($location_block){?>
        <section class="contact-us location"> 
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <h2 class="section-name"><?php echo $location_block['block_title'];?></h2>
                    </div>

                    <div class="col-lg-9">
                        <div class="text">
                            <?php echo $location_block['text'];?>
                            <?php $apartments_logo = $location_block['apartments_logo'];
                            if($apartments_logo){?>
                                <div class="apartments-logos text-right">
                                    <ul class="d-md-flex justify-content-lg-end">
                                        <?php foreach($apartments_logo as $logo){?>
                                            <li>
                                                <a href="<?php echo $logo['link'];?>">
                                                    <?php $logo_thumb = $logo['logo'];
                                                    $image_sizes = 'logo_thumb';
                                                    $image_urls = $logo_thumb['sizes'][$image_sizes];?>
                                                    <img src="<?php echo $image_urls;?>">
                                                </a>
                                            </li>
                                        <?php };?>
                                    </ul>
                                </div>
                            <?php };?>
                        </div>
                    </div>
                </div>

                <div class="map">
                    <?php $map = $location_block['coordinates'];
                    if($map){?>
                        <div id="google-map"></div>

                        <script>
                            function initMap() {

                                var map = new google.maps.Map(document.getElementById('google-map'), {
                                    zoom: 15,
                                    center: {lat: <?php echo $map['latitude'];?>, lng: <?php echo $map['longitude'];?>},
                                    styles:[
                                        {
                                            "elementType": "geometry",
                                            "stylers": [
                                              {
                                                "color": "#f5f5f5"
                                              }
                                            ]
                                        },
                                        {
                                            "elementType": "labels.icon",
                                            "stylers": [
                                              {
                                                "visibility": "off"
                                              }
                                            ]
                                        },
                                        {
                                            "elementType": "labels.text.fill",
                                            "stylers": [
                                              {
                                                "color": "#616161"
                                              }
                                            ]
                                        },
                                        {
                                            "elementType": "labels.text.stroke",
                                            "stylers": [
                                              {
                                                "color": "#f5f5f5"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "administrative.land_parcel",
                                            "elementType": "labels.text.fill",
                                            "stylers": [
                                              {
                                                "color": "#bdbdbd"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "poi",
                                            "elementType": "geometry",
                                            "stylers": [
                                              {
                                                "color": "#eeeeee"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "poi",
                                            "elementType": "labels.text.fill",
                                            "stylers": [
                                              {
                                                "color": "#757575"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "poi.park",
                                            "elementType": "geometry",
                                            "stylers": [
                                              {
                                                "color": "#e5e5e5"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "poi.park",
                                            "elementType": "labels.text.fill",
                                            "stylers": [
                                              {
                                                "color": "#9e9e9e"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "road",
                                            "elementType": "geometry",
                                            "stylers": [
                                              {
                                                "color": "#ffffff"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "road.arterial",
                                            "elementType": "labels.text.fill",
                                            "stylers": [
                                              {
                                                "color": "#757575"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "road.highway",
                                            "elementType": "geometry",
                                            "stylers": [
                                              {
                                                "color": "#ffffff"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "road.highway",
                                            "elementType": "labels.text.fill",
                                            "stylers": [
                                              {
                                                "color": "#616161"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "road.local",
                                            "elementType": "labels.text.fill",
                                            "stylers": [
                                              {
                                                "color": "#9e9e9e"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "transit.line",
                                            "elementType": "geometry",
                                            "stylers": [
                                              {
                                                "color": "#e5e5e5"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "transit.station",
                                            "elementType": "geometry",
                                            "stylers": [
                                              {
                                                "color": "#eeeeee"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "water",
                                            "elementType": "geometry",
                                            "stylers": [
                                              {
                                                "color": "#c9c9c9"
                                              }
                                            ]
                                        },
                                        {
                                            "featureType": "water",
                                            "elementType": "labels.text.fill",
                                            "stylers": [
                                              {
                                                "color": "#9e9e9e"
                                              }
                                            ]
                                        }
                                    ]
                   
                                });

                                var beachMarker = new google.maps.Marker({
                                    position: {lat: <?php echo $map['latitude'];?>, lng: <?php echo $map['longitude'];?>},
                                    map: map
                                });
                            }
                        </script>
                    <?php };?>
                    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1Jr1HTNuJabgm7KSBykcHUdQYj5ZdOJk&callback=initMap"></script>
                </div>

                <a href="#" class="scroll-down blue-arrow left"></a>
            </div>
        </section>
    <?php };?>
    <?php require get_template_directory() . '/views/contact-form.php';?>

</main>
<?php get_footer(); ?>

