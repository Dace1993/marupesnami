<?php 
/*
Template Name: Contacts
*/ get_header(); ?>
<main id="main-content">
    <div class="container">
        <h1 class="page-name"><?php the_title();?></h1>
        <?php $coordinates_addresses = get_field('coordinates_addresses');
        if($coordinates_addresses){?>
            <section class="contact-us addresses"> 
                <div class="row">
                    <?php $count = 0;
                    foreach($coordinates_addresses as $coor_addr){
                        $count++;?> 
                        <div class="col-lg-6">
                            <div class="address-block">
                                <h2 class="block-name"><?php echo $coor_addr['block_title'];?></h2>
                                <p class="address"><?php echo $coor_addr['address'];?></p>

                                <div class="map">
                                    <div id="google-map<?php if($count==2){echo '-'.$count;};?>"></div>                            
                                </div>
                            </div>
                        </div>
                    <?php };?>
                   
                </div>
                
            </section>
        <?php };?>
        <script type="text/javascript">
            function initMap() {
                <?php $coordinates_addresses = get_field('coordinates_addresses');
                if($coordinates_addresses){
                    $counter = 0;
                    foreach($coordinates_addresses as $coor_addr){
                        $counter++;?> 
                        var map<?php if($counter==2){echo $counter;};?> = new google.maps.Map(document.getElementById('google-map<?php if($counter==2){echo '-'.$counter;};?>'), {
                            zoom: 15,
                            center: {lat:<?php echo $coor_addr['Latitude'];?>, lng: <?php echo $coor_addr['longitude'];?>},
                            styles:[
                                {
                                    "elementType": "geometry",
                                    "stylers": [
                                      {
                                        "color": "#f5f5f5"
                                      }
                                    ]
                                },
                                {
                                    "elementType": "labels.icon",
                                    "stylers": [
                                      {
                                        "visibility": "off"
                                      }
                                    ]
                                },
                                {
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                      {
                                        "color": "#616161"
                                      }
                                    ]
                                },
                                {
                                    "elementType": "labels.text.stroke",
                                    "stylers": [
                                      {
                                        "color": "#f5f5f5"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "administrative.land_parcel",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                      {
                                        "color": "#bdbdbd"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "poi",
                                    "elementType": "geometry",
                                    "stylers": [
                                      {
                                        "color": "#eeeeee"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "poi",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                      {
                                        "color": "#757575"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "poi.park",
                                    "elementType": "geometry",
                                    "stylers": [
                                      {
                                        "color": "#e5e5e5"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "poi.park",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                      {
                                        "color": "#9e9e9e"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "geometry",
                                    "stylers": [
                                      {
                                        "color": "#ffffff"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                      {
                                        "color": "#757575"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "geometry",
                                    "stylers": [
                                      {
                                        "color": "#ffffff"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                      {
                                        "color": "#616161"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "road.local",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                      {
                                        "color": "#9e9e9e"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "transit.line",
                                    "elementType": "geometry",
                                    "stylers": [
                                      {
                                        "color": "#e5e5e5"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "transit.station",
                                    "elementType": "geometry",
                                    "stylers": [
                                      {
                                        "color": "#eeeeee"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "geometry",
                                    "stylers": [
                                      {
                                        "color": "#c9c9c9"
                                      }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                      {
                                        "color": "#9e9e9e"
                                      }
                                    ]
                                }
                            ]
           
                        });
                        var beachMarker = new google.maps.Marker({
                            position: {lat: <?php echo $coor_addr['Latitude'];?>, lng: <?php echo $coor_addr['longitude'];?>},
                            map: map<?php if($counter==2){echo $counter;};?>
                        });
                    <?php };?>
                <?php };?>
                               
            }
        </script> 
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1Jr1HTNuJabgm7KSBykcHUdQYj5ZdOJk&callback=initMap"></script>
    </div>

    <?php require get_template_directory() . '/views/contact-form.php';?>
</main>

<?php get_footer();?>
