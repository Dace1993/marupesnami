<?php get_header(); ?>
    <main id="main-content">
        <div class="container">
            <div class="post-view">
                <article class="post">
                    <header class="entry-header">
                        <h1 class="entry-title"><?php the_title();?></h1>
                    </header>
                    <div class="entry-content">
                        <?php if(have_posts()) : ?> 
                            <?php while ( have_posts()) : the_post(); ?>
                              <?php the_content();?>
                            <?php endwhile;
                        endif;?>
                        <?php $accordion_or_tabs = get_field('accordion_or_tabs');
                        if($accordion_or_tabs){
                            $count = 0;
                            foreach($accordion_or_tabs as $repeater){
                                $count++;?>
                                <div class="block">
                                    <h2><?php echo $repeater['title'];?></h2>
                                    <?php $accordions = $repeater['choose'];
                                    $array = array();
                                    if($accordions){
                                        foreach($accordions as $accordion){
                                            $accordion;
                                            array_push($array, $accordion);
                                        };
                                    };
                                    $accordion_value = 'accordion';
                                    if( in_array($accordion_value,$array)){ ?>
                                        <div id="accordion<?php echo $count;?>" class="accordion" >
                                            <?php $accordion_choose = $repeater['accordion_tabs_block'];
                                            if($accordion_choose){
                                                $i = 0;
                                                foreach($accordion_choose as $choosed){
                                                    $i++;?>
                                                    <div class="card">
                                                        <div class="card-header" id="heading-<?php echo $i;?>">
                                                            <h5 class="mb-0">
                                                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse-<?php echo $i;?>" aria-expanded="<?php if($i==1){echo 'true';}else{echo 'false';};?>" aria-controls="collapse-<?php echo $i;?>"><?php echo $choosed['title'];?> <i class="fas fa-caret-down d-flex justify-content-center align-items-center"></i></button>
                                                            </h5>
                                                        </div>

                                                        <div id="collapse-<?php echo $i;?>" class="collapse <?php if($i==1){echo 'show';};?>" aria-labelledby="heading-<?php echo $i;?>" data-parent="#accordion<?php echo $count;?>">
                                                            <div class="card-body"><?php echo $choosed['text'];?></div>
                                                        </div>
                                                    </div>
                                                <?php }
                                            };?>
                                        </div>
                                    <?php }else{?>
                                        <div class="tabs-block">
                                            <ul class="nav nav-tabs" id="myTab<?php echo $count;?>" role="tablist">
                                                <?php $tabs_choose = $repeater['accordion_tabs_block'];
                                                if($tabs_choose){
                                                    $x = 0;
                                                    foreach($tabs_choose as $choosed_tabs){
                                                        $x++;?>
                                                        <li class="nav-item">
                                                            <a class="nav-link <?php if($x==1){echo 'active';};?>" data-toggle="tab" href="#tab<?php echo $count;?>-<?php echo $x;?>" role="tab" aria-expanded="<?php if($x==1){echo 'true';}else{echo 'false';};?>"><?php echo $choosed_tabs['title'];?></a>
                                                        </li>
                                                    <?php };
                                                };?>
                                                
                                            </ul>

                                            <div class="tab-content">
                                                <?php $choose_tabs = $repeater['accordion_tabs_block'];
                                                if($choose_tabs){
                                                    $y = 0;
                                                    foreach($choose_tabs as $tabs){
                                                        $y++;?>
                                                        <div class="tab-pane fade <?php if($y==1){echo 'show active';};?>" id="tab<?php echo $count;?>-<?php echo $y;?>" role="tabpanel">
                                                            <?php echo $tabs['text'];?>
                                                        </div>
                                                    <?php };
                                                };?>
                                            </div>
                                        </div>
                                    <?php };?>
                                </div>
                            <?php };
                        };?>
                    </div>
                </article>
            </div>
        </div>
    </main>
<?php get_footer();?>
