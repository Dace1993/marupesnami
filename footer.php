        <footer id="main-footer">
            <div class="container">
                <div class="row">
                    <div class="logo-col col-lg-4">
                        <?php $footer_logo = get_field('footer_logo','option');
                        $image_size = 'footer-logo_thumb';
                        $image_url =  $footer_logo['sizes'][$image_size];?>
                        <?php if($footer_logo){?>
                            <img src="<?php echo $image_url;?>" alt="Marupes Nami" class="logo-footer">
                        <?php };?>
                        <div class="address">
                            <?php $footer_address = get_field("footer_address","option");
                            echo _e($footer_address,'marupesnami');?>
                        </div>
                    </div>
                    <?php $partners = get_field('partners','option');
                    if($partners){?>
                        <div class="col-lg-3">
                            <div class="d-lg-flex">
                                <h3 class="col-head"><?php echo _e('Partneri','marupesnami');?></h3>
                                
                                <ul class="partners">
                                    <?php foreach($partners as $partner){?>
                                        <?php $partners_thumb = $partner['image'];
                                        $images_sizes = 'partners_thumb';
                                        $images_urls = $partners_thumb['sizes'][$images_sizes];?>
                                       <li><img src="<?php echo $images_urls;?>"></li>
                                    <?php };?> 
                                </ul>
                            </div>

                        </div>
                    <?php };?>
                    <?php $social_media = get_field('social_media','option');
                    if($social_media){?>
                        <div class="col-lg-3">
                            <div class="d-lg-flex">
                                <h3 class="col-head"><?php echo _e('Seko mums','marupesnami');?></h3>

                                <ul class="social-media">
                                    <?php foreach ($social_media as $media) {?>
                                        <li>
                                            <a href="<?php echo $media['link'];?>" target="_blank">
                                                <?php $media_thumb = $media['icon'];
                                                $images_size = 'social-media_thumb';
                                                $images_url = $media_thumb['sizes'][$images_size];?>
                                                <img src="<?php echo $images_url;?>">
                                            </a>
                                        </li>
                                    <?php };?>
                                </ul>
                            </div>
                        </div>
                    <?php };?>
                    <div class="col-lg-2 d-lg-flex align-items-end justify-content-end">
                        <span class="copyrights"><?php the_field('copyright','option');?></span>
                    </div>
                </div>
            </div>
        </footer>
        
        <div id="nav-overlay"></div>

        <!-- JS files -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/app/assets/owlcarousel/owl.carousel.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/app/assets/js/functions.js"></script>

        <!--IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?php echo get_template_directory_uri(); ?>/app/assets/js/ie10-viewport-bug-workaround.js"></script>
        <?php wp_footer(); ?>
    </body>
</html>
