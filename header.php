<!DOCTYPE html>
<html lang="lv">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/app/assets/owlcarousel/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/app/assets/owlcarousel/owl.theme.default.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="<?php echo get_template_directory_uri(); ?>/app/assets/css/style.css" rel="stylesheet" />
        <title><?php wp_title( '|', true, 'right' ); ?></title>
    </head>

    <body <?php body_class('page'); ?>>
        <header id="main-header">
            <nav class="navbar">
                <div class="container d-flex align-items-center justify-content-start">
                    <?php $header_logo = get_field('header_logo','option');
                    $image_size = 'header-logo_thumb';
                    $image_url =  $header_logo['sizes'][$image_size];?>
                    <?php if($header_logo){?>
                        <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                            <img src="<?php echo $image_url; ?>" alt="Marupes Nami" />
                        </a>
                    <?php };?>
                    <div class="nav-wrap">
                        <ul class="navbar-nav nav d-flex flex-row">
                            <?php
                                wp_nav_menu( array(
                                    'menu'              => 'Top menu',
                                    'theme_location'    => 'top_navigation',
                                    'depth'             => 3,
                                    'container'         => true,
                                    'container_class'   => 'nav-item btn-li',
                                    'container_id'      => '',
                                    'menu_class'        => 'navbar-nav nav d-flex flex-row',
                                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                    'walker'            => new wp_bootstrap_navwalker())

                                );
                            ?>
                        </ul>

                        <a href="#" class="close-menu"></a>
                    </div>

                    <a href="#" class="menu-toggle">
                        <i class="icon"></i>
                    </a>
                </div>

                <div class="lang-bar">
                    <ul>
                        <?php pll_the_languages(); ?>
                    </ul>
                </div>
            </nav>
            <?php wp_head(); ?>
             <?php wp_reset_query();?>
        </header>
        <?php $available_flats = get_field('available_flats','option');
        if($available_flats){?>
            <div class="sticky-banner">
                <a href="#" class="d-flex" id="ask-consultation">
                    <span class="amount d-flex align-items-start"><i class="badge d-flex align-items-center justify-content-center">
                <?php       $tax_query = array('relation' => 'AND');
                $tax_query[] =  array(
                        'taxonomy' => 'statuss',
                        'field' => 'slug',
                        'terms' => 'brivs',
                    );
  
                $args = array(
                    'posts_per_page'    => 100,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'flats_posts',
                    'post_status'      => 'publish',
                    'tax_query' => $tax_query,
                     
                );
                $query = new WP_Query($args);
                $post_count = $query->post_count;
                $custom_posts = get_posts($args);
                $counter=0; 
                    $len = count($custom_posts);
                    if($query->have_posts()) {
                        while ( $query->have_posts()) { $query->the_post();
                            $counter++;
                            if ($counter == $len) {
                                echo $counter.' ';?></i></span><?php echo _e('Piesakies konsultācijai','marupesnami').'! ';
                                if($counter>1){echo  _e('Vēl pieejami','marupesnami');}else{echo _e('Vēl pieejams','marupesnami');};echo ' '.$counter.' '; echo _e('dzīvokļi','marupesnami');?></a>
                            <?php };

                        };
                    };
                ?>
                    
            </div>
        <?php };
        wp_reset_query();?>