<?php
/**
 * SW engine. 
 * Global Template functions.
 */

require get_template_directory() . '/inc/base.php';

/**
 * Structure.
 * Template functions used throughout the theme.
 */

require get_template_directory() . '/inc/theme.php';

/**
 * Widgets.
 * Template widgets used throughout the theme.
 */

require get_template_directory() . '/inc/widgets.php';


function simple_pagination($pages = '', $range = 2)
{  
   $showitems = ($range * 2)+1;  

   global $paged;
    if(empty($paged)) $paged = 1;

   if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }  

   if(1 != $pages)
    {?>

                                                <nav aria-label="Page navigation">

<ul class="pagination justify-content-center align-items-center flex-wrap">
    <?php if($paged > 1 ){
           echo '<li class="page-item"> <a class="page-link prev d-flex align-items-center justify-content-center" href="'.get_pagenum_link($paged - 1).'"><i class="fas fa-caret-left"></i></a></li>';
        }
         else {
             echo '<li class="page-item disabled prev"> <a class="page-link" href="'.get_pagenum_link(1).'"><i class="fas fa-caret-left"></i></a></li>';
            }
    for ($i=1; $i <= $pages; $i++){
        if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
            echo ($paged == $i)? '<li class="page-item "><a class="page-link active" href="'.get_pagenum_link($i).'">'.$i.'</a></li>':'<li class="page-item"><a class="page-link" href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
        }
    }
    if ($paged < $pages){
           //echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";
           echo '<li class="page-item"><a class="page-link next d-flex align-items-center justify-content-center" href="'.get_pagenum_link($paged + 1).'"><i class="fas fa-caret-right"></i></a></li>';
        }
        else{
          echo '<li class="page-item disabled next"><a class="page-link " href="'.get_pagenum_link($paged + 1).'"><i class="fas fa-caret-right"></i></a></li>';
        }?>

</ul>
       <
   <?php     
 
    }
}
function my_custom_styles( $init_array ) {  
 
// Define the style_formats array
 
    $style_formats = array(  
/*
* Each array child is a format with it's own settings
* Notice that each array has title, block, classes, and wrapper arguments
* Title is the label which will be visible in Formats menu
* Block defines whether it is a span, div, selector, or inline style
* Classes allows you to define CSS classes
* Wrapper whether or not to add a new block-level element around any selected elements
*/
        array(  
            'title' => 'List style',  
            'selector' =>'ul',
            'classes' => 'check',
            'wrapper' => true,
             
        ),
        array(  
            'title' => 'List style 2',  
            'selector' =>'ul',
            'classes' => 'caret-right',
            'wrapper' => true,
             
        ),
        array(  
            'title' => 'Button style',  
            'selector' =>'a',
            'classes' => 'btn btn-primary',
            'wrapper' => false,
             
        ),
        array(  
            'title' => 'Alert style',  
            'selector' =>'p',
            'classes' => 'alert alert-information',
            'wrapper' => false,             
        ),
        array(  
            'title' => 'Alert style 2',  
            'selector' =>'p',
            'classes' => 'alert alert-tip',
            'wrapper' => false,             
        ),
    );  
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );  
     
    return $init_array;  
   
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_custom_styles' ); 
function add_style_select_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'add_style_select_buttons' );

function custom_editor_styles() {
  add_editor_style('editor-styles.css');
}
 
add_action('init', 'custom_editor_styles');


function register_strings() {

$terms_statuss = get_terms('statuss');
    foreach ( $terms_statuss as $tax_statuss ) {
        pll_register_string('marupesnami', $tax_statuss->name);
    }
    $footer_address = get_field('footer_address','option');
    pll_register_string('marupesnami', $footer_address);
    pll_register_string('marupesnami', $tax_statuss->name);
    $contact_persons = get_field('contact_persons','option');
    if($contact_persons){
        foreach($contact_persons as $person){
            $person['position'];
            $person['name_surname'];
            pll_register_string('marupesnami', $person['name_surname']);
            pll_register_string('marupesnami', $person['position']);
        };
    };
}
add_action('init','register_strings');


add_shortcode( 'gallery', 'modified_gallery_shortcode' );

function modified_gallery_shortcode($attr){
    // Replace WP gallery with OWL Carousel using gallery shortcode -- just add `owl=true`
    //
    // [gallery owl="true" link="none" size="medium" ids="378,377,376,375,374,373"]

    if( isset($attr['owl'])||!isset($attr['owl'])) {

        $output = gallery_shortcode($attr);

        $output = strip_tags($output,'<img><span>'); // remove extra tags, but keep these
        $output = str_replace('gallery-item', "item", $output); // remove class attribute

        $output = " <div class=\"flat-overview\"><section class=\"gallery\" style=\"background:unset;padding:45px 0;\"><div class=\"container\"> <div class=\"owl-carousel\">$output</div></div></section></div>"; // wrap in div

        // begin styles and js

  
    }
    else{
        // default gallery
        $output = gallery_shortcode($attr);
    }

    return $output; 
}