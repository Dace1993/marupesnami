<?php 
/*
Template Name: News
*/
get_header(); ?>
<main id="main-content"> 
    <div class="container">
        <div class="posts-list">
            <?php $tax_query = array();      
                $args = array(
                    'post_type' => 'post',
                    'group_by' => 'date',
                    'order' => 'DESC',
                    'tax_query' => $tax_query,
                    'paged' => $paged,
                ); 
                $query = new WP_Query($args);
                $page_count = $query->max_num_pages;

            ?>

            <?php if($query -> have_posts()) : ?> 
                <?php while ( $query->have_posts()) : $query->the_post(); ?>
                    <article class="post d-md-flex align-items-center">
                        <div class="featured-image">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail(); ?>
                            </a>
                        </div>

                        <div class="details">
                            <h2 class="entry-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
                            </h2>

                            <p class="excerpt"><?php echo wp_strip_all_tags(excerpt(86)) ?></p>

                            <a href="<?php the_permalink(); ?>" class="view-more"><?php echo _e('Show post');?></a>
                        </div>
                    </article>
                <?php endwhile;
            endif;?>
        </div>
        <?php if($page_count >1 ){?>
            <?php simple_pagination(); ?>
        <?php };?>

    </div>
</main>
<?php get_footer();?>