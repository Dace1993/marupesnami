<?php
function bklatvia_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar', 'bklatvia' ),
		'id' => 'sidebar',
		'description' => __( '', 'bklatvia' ),
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer', 'bklatvia' ),
		'id' => 'footer',
		'description' => __( '', 'bklatvia' ),
		'before_widget' => '<div class="col-lg-4 col-md-4">',
		'after_widget' => '</div>',
		'before_title' => '<span class="col-head">',
		'after_title' => '</span>',
	) );

	

	
}
add_action( 'widgets_init', 'bklatvia_widgets_init' );
?>