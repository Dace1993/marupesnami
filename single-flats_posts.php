<?php get_header(); ?>
<main id="main-content">
    <div class="flat-overview">
        <section class="details">
            <div class="container">
                <h1 class="page-name"><?php echo _e('Dzīvoklis','marupesnami').' #'; echo get_field('flats_number');?></h1>
                
                <div class="text">
                    <?php if(have_posts()) : ?> 
                        <?php while ( have_posts()) : the_post(); ?>
                          <?php the_content();?>
                        <?php endwhile;
                    endif;?>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="image">
                            <?php the_post_thumbnail('product_thumb');?>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="summary">
                            <div class="d-sm-flex justify-content-between align-items-center">
                                <div class="rooms">
                                    <?php $terms_istabas = get_the_terms( get_the_ID(), 'istabas' );
                                    $array = array();
                                    $array_slug = array();
                                    foreach($terms_istabas as $rooms_term){
                                        echo $rooms_name = $rooms_term->name;
                                        $rooms_name = $rooms_term->name;
                                        array_push($array,$rooms_name);
                                        $rooms_slug = $rooms_term->slug;
                                        array_push($array_slug,$rooms_slug);
                                    };
                                    $one = '1';
                                    if(!in_array($one,$array)){?>
                                        <?php echo _e('Istabas','marupesnami');?>
                                    <?php }else{?>
                                        <?php echo _e('Istaba','marupesnami');?>
                                    <?php };?>
                                </div>
                                <?php $tax_query = array('relation' => 'AND');
                                    $tax_query[] =  array(
                                    'taxonomy' => 'statuss',
                                    'field' => 'slug',
                                    'terms' => 'brivs',
                                );
                                 $tax_query[] =  array(
                                    'taxonomy' => 'istabas',
                                    'field' => 'slug',
                                    'terms' => $rooms_term->slug,
                                );
                                $args = array(
                                    'posts_per_page'    => 100,
                                    'orderby'          => 'date',
                                    'order'            => 'DESC',
                                    'post_type'        => 'flats_posts',
                                    'post_status'      => 'publish',
                                    'lang'             => pll_default_language(),
                                    'tax_query'=>$tax_query,
                                );
                                $query = new WP_Query($args);
                                $post_count = $query->post_count;

                                if($post_count > 1){
                                    echo '<div class="total">'; echo _e('Pieejami','marupesnami').' '.$post_count.' '; echo _e('dzīvokļi','marupesnami').'</a></div>';
                                }else{
                                    echo '<div class="total">'; echo _e('Pieejams','marupesnami').' '.$post_count.' '; echo _e('dzīvoklis','marupesnami').'</a></div>';
                                };?> 
                            </div>

                            <div class="description">
                                 <p><?php the_field('flats_short_description');?></p>
                            </div>

                            <ul class="features">
                                <?php if(get_field('area_number')){?>
                                    <li>
                                        <?php echo _e('Platība','marupesnami'); echo ': '.get_field('area_number').' m2';
                                        ?>   
                                    </li>
                                <?php };?>
                                <li><?php echo _e('Pieejamie stāvi','marupesnami');?>: 
                                    <?php $terms_stavs = get_the_terms( get_the_ID(), 'stavs' );
                                    $n = 0;
                                    $len = count($terms_stavs);
                                    foreach($terms_stavs as $stavs_term){
                                        $n++;
                                        if ($n == $len) {
                                            echo $stavs_term->name.'.';
                                        }else{
                                            echo $stavs_term->name.'., ';
                                        }
                                    };?>         
                                </li>
                            </ul>

                            <div class="price text-lg-right">
                                <strong><?php echo _e('CENA NO','marupesnami');?>: <?php the_field('flats_price');?> Eur</strong>
                            </div>

                            <div class="d-lg-flex justify-content-end">
                                <a href="#" class="btn btn-primary" id="ask-consultation"><?php echo _e('PIESAKIES KONSULTĀCIJAI','marupesnami');?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php $flats_gallery = get_field('flats_gallery');
        if($flats_gallery){?>
            <section class="gallery">
                <div class="container">
                    <div class="owl-carousel">
                        <?php foreach($flats_gallery as $gallery){?>
                            <div>
                                <?php $gallery_image = $gallery['image'];
                                $gallery_size = 'gallery_thumb';
                                $url_img = $gallery_image['sizes'][$gallery_size];?>
                                <img src="<?php echo $url_img;?>" alt="">
                            </div>
                        <?php };?>
                    </div>

                    <a href="#" class="scroll-down white-arrow left"></a>
                </div>
            </section>
        <?php };?>
        
        <section class="rooms-2d">
            <div class="container">
                <h2 class="section-name"><?php echo _e('Dzīvokļa novietojums','marupesnami');?></h2>
                
                <div class="html-map">
                    <i class="directions-icon"></i>
                    <?php $person_thumb = get_field('apartment_location');
                    $image_size = 'apartment_location_thumb';
                    $image_url = $person_thumb['sizes'][ $image_size];?>
                    <img src="<?php echo $image_url;?>" id="map-image" style="width: 997px; max-width: 100%; height: auto;" alt="" usemap="#map" />

                    <map name="map">
                        <area shape="poly" coords="536, 41, 399, 41, 399, 182, 367, 182, 367, 251, 401, 251, 401, 303, 538, 303" />
                        <area shape="poly" coords="190, 302, 190, 183, 257, 183, 257, 303" />
                        <area shape="poly" coords="44, 69, 44, 255, 180, 255, 182, 128, 326, 125, 327, 39, 189, 40, 190, 69" />
                    </map>
                </div>

                <a href="#" class="scroll-down green-arrow right"></a>
            </div>
        </section>
    </div>

    <?php require get_template_directory() . '/views/contact-form.php';?>

</main>
<?php get_footer();?>