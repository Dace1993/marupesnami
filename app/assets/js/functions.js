$(function(){

	/* Sticky banner */
	$( ".check li").prepend( "<i class='fas fa-check'></i> " );
	$( ".caret-right li").prepend( "<i class='fas fa-caret-right'></i> " );

	if($('.sticky-banner').length) {
		var stickyBannerWidth = $('.sticky-banner').width() - 80;

		$('.sticky-banner').css('right', '-' + stickyBannerWidth + 'px');
	}

	/* Sticky banner */

	/* Header dropdown on hover */

	$('#main-header .navbar-nav .dropdown').hover(            
	    function() {
	    	if($(window).width() > 1199) {
				$('.dropdown-menu, .sub-menu', this).stop( true, true ).fadeIn("fast");
		        $(this).toggleClass('show');   
	        }            
	    },

	    function() {
	    	if($(window).width() > 1199) {
	        	$('.dropdown-menu, .sub-menu', this).stop( true, true ).fadeOut("fast");
	        	$(this).toggleClass('show');     
	        } 
	   	}
	);

	/* /Header dropdown on hover */

	/* Call the OWL */

	if($('.hero-slider').length) {
		$('.hero-slider .owl-carousel').owlCarousel({
			items: 1,
			margin: 0,
			autoplay: true,
			autoplayTimeout: 4000,
			loop: true,
    		nav: false,
    		dots: true
		});
	}

	if($('.gallery .owl-carousel').length) {
		$('.gallery .owl-carousel').owlCarousel({
			items: 1,
			margin: 0,
			autoplay: true,
			loop: true,
    		nav: true,
    		dots: false
		});
	}

	/* /OWL */

	/* Smooth scrolling */

	$('.scroll-down').on('click', function(){
		$('html, body').animate({scrollTop: $(this).closest('section').offset().top - 56}, 500);
			
		return false;
	});


	$('.flat-overview #ask-consultation, .sticky-banner #ask-consultation').on('click', function(){
		$('html, body').animate({scrollTop: $('.contact-us.form').offset().top}, 500);
			
		return false;
	});

	/* Smooth scrolling */


	/* RWD Menu */

 	$('#main-header .menu-toggle').on('click', function() {
 		$('#main-header').addClass('opened-nav');
 		$(this).toggleClass('opened');
 		
 		if($(this).hasClass('opened')) {
		 	if(!$('#main-header .nav-wrap .lang-bar').length) {
			 	$('#main-header .nav-wrap').prepend($('#main-header .lang-bar').prop('outerHTML'));
		 	} 
		 	
			$('#nav-overlay, #main-header .nav-wrap').addClass('active');
 			$('#main-header .nav-wrap').animate({ right: 0 }, 350);

		} else {
			$('#nav-overlay, #main-header .nav-wrap').removeClass('active');
 			$('#main-header .nav-wrap').animate({ right:'-100%' }, 250);
		}
 				
 		return false;
 	});

 	$(document).on('click', '#main-header .nav-wrap .close-menu, #nav-overlay', function(){
 		$('#main-header').removeClass('opened-nav');
 		$('#main-header .menu-toggle').removeClass('opened');
		$('#nav-overlay').removeClass('active');
		$('#main-header .nav-wrap').animate({right:'-100%' }, 250);	

		return false;
	});

});

